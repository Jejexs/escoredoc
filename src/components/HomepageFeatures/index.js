import clsx from 'clsx';
import Heading from '@theme/Heading';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Suivi des Matchs en Direct',
    imageUrl: '/img/vs.png',
    description: (
      <>
        E-Score vous permet de suivre les matchs de vos jeux favoris en temps réel, avec des mises à jour instantanées des scores et des résultats.
      </>
    ),
  },
  {
    title: 'Actualités E-Sport',
    imageUrl: '/img/actualites-en-direct.png',
    description: (
      <>
        Restez informé des dernières actualités et tendances du monde de l'e-sport grâce à notre blog dédié.
      </>
    ),
  },
  {
    title: 'Calendrier Complet',
    imageUrl: '/img/time-and-calendar.png',
    description: (
      <>
        Consultez le calendrier complet des matchs passés, en cours et à venir pour ne jamais manquer un événement important.
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <img src={imageUrl} className={styles.featureImg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <Heading as="h3">{title}</Heading>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
