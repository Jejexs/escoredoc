---
sidebar_position: 1
id: introduction
title: Introduction
---

# Introduction

Bienvenue dans la documentation de l'application **E-Score**.

Cette documentation vous fournira toutes les informations nécessaires pour installer et utiliser **E-Score**, ainsi qu'une documentation détaillée du code source. 

Que vous soyez un développeur ou un utilisateur, vous trouverez ici toutes les ressources dont vous avez besoin pour tirer le meilleur parti de cette application.
