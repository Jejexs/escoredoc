---
id: guide-utilisateur
title: Guide Utilisateur
sidebar_position: 10
---

# Guide Utilisateur

Pour télécharger le guide utilisateur complet en PDF, cliquez sur le lien ci-dessous :

[Télécharger le guide utilisateur ici](/img/guide/guide-utilisateur.pdf)

## Page 1 | Comment s'inscrire
![Page 1](/img/guide/1.png)

## Page 2
![Page 2](/img/guide/2.png)

## Page 3
![Page 3](/img/guide/3.png)

## Page 4 | Barre de navigation
![Page 4](/img/guide/4.png)

## Page 5
![Page 5](/img/guide/5.png)

## Page 6
![Page 6](/img/guide/6.png)

## Page 7
![Page 7](/img/guide/7.png)

## Page 8 | Page calendrier
![Page 8](/img/guide/8.png)

## Page 9
![Page 9](/img/guide/9.png)

## Page 10
![Page 10](/img/guide/10.png)

## Page 11
![Page 11](/img/guide/11.png)

## Page 12
![Page 12](/img/guide/12.png)

Grâce à ce guide, vous pouvez maîtriser tout le nécessaire pour profiter pleinement de l'application E-Score.

---