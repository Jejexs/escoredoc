---
id: private-route
title: PrivateRoute
sidebar_position: 4
---

# PrivateRoute

## Description

Le composant `PrivateRoute` est utilisé pour protéger les routes qui nécessitent une authentification. Il redirige vers la page de connexion si l'utilisateur n'est pas authentifié.

## Code

```javascript
import React, { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';

/**
 * Composant pour les routes privées qui nécessite une authentification.
 * Redirige vers la page de connexion si l'utilisateur n'est pas authentifié.
 * @param {Object} props - Les propriétés du composant.
 * @param {React.ReactNode} props.children - Les composants enfants à afficher si l'utilisateur est authentifié.
 * @returns {React.ReactNode} - Les composants enfants ou une redirection vers la page de connexion.
 */
const PrivateRoute = ({ children }) => {
  const authContext = useContext(AuthContext); // Utilisation du contexte d'authentification

  // Vérifie si le contexte d'authentification est disponible
  if (!authContext) {
    console.error('Auth context is not available');
    return <Navigate to="/login" />;
  }

  const { isAuthenticated } = authContext; // Obtient l'état d'authentification depuis le contexte

  // Retourne les enfants si l'utilisateur est authentifié, sinon redirige vers la page de connexion
  return isAuthenticated ? children : <Navigate to="/login" />;
};

export default PrivateRoute;
```

## Explication du Code

1. **Importations** :
    - `React` : Bibliothèque principale pour construire des interfaces utilisateur.
    - `Navigate` : Composant de `react-router-dom` pour la redirection.
    - `AuthContext` : Contexte d'authentification.

2. **Propriétés du Composant** :
    - `children` : Composants enfants à afficher si l'utilisateur est authentifié.

3. **Fonction PrivateRoute** :
    - **useContext(AuthContext)** : Obtient le contexte d'authentification.
    - **Vérification de l'authentification** : Si le contexte ou l'état d'authentification n'est pas disponible, redirige vers la page de connexion avec `Navigate`.

4. **Retour** :
    - Affiche les enfants si l'utilisateur est authentifié, sinon redirige vers la page de connexion.

## Conclusion

Le composant `PrivateRoute` protège les routes nécessitant une authentification en redirigeant les utilisateurs non authentifiés vers la page de connexion, en utilisant le contexte d'authentification et `react-router-dom` pour la navigation.

---