---
id: back-button
title: BackButton
sidebar_position: 1
---

# Composant BackButton

## Description

Le composant `BackButton` est utilisé pour permettre à l'utilisateur de revenir à la page précédente. Il affiche une icône de flèche gauche et le texte "Retour".

## Code

```javascript
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { FaArrowLeft } from 'react-icons/fa';

const BackButton = () => {
    const navigate = useNavigate();

    return (
        <button
            onClick={() => navigate(-1)}
            className="flex items-center text-white bg-transparent border border-white hover:bg-white hover:text-black font-bold py-2 px-4 rounded-full transition duration-300"
        >
            <FaArrowLeft className="mr-2" />
            Retour
        </button>
    );
};

export default BackButton;
```

## Explication du Code

1. **Importations** :
    - `React` : Bibliothèque principale pour construire des interfaces utilisateur.
    - `useNavigate` : Hook de `react-router-dom` pour naviguer entre les pages.
    - `FaArrowLeft` : Icône de flèche gauche de `react-icons`.

2. **Fonction BackButton** :
    - **Hook useNavigate** : Initialise une fonction de navigation pour revenir à la page précédente.
    - **Bouton** :
        - **onClick** : Lorsque l'utilisateur clique sur le bouton, la fonction `navigate(-1)` est appelée pour revenir en arrière.
        - **Classes Tailwind CSS** : 
            - `flex` : Affiche les éléments en ligne et centre les éléments verticalement.
            - `items-center` : Centre les éléments sur l'axe vertical.
            - `text-white` : Couleur du texte en blanc.
            - `bg-transparent` : Fond transparent.
            - `border` : Ajoute une bordure.
            - `border-white` : Bordure blanche.
            - `hover:bg-white` : Change le fond en blanc au survol.
            - `hover:text-black` : Change la couleur du texte en noir au survol.
            - `font-bold` : Texte en gras.
            - `py-2` : Padding vertical de 0.5rem.
            - `px-4` : Padding horizontal de 1rem.
            - `rounded-full` : Bordures arrondies.
            - `transition` et `duration-300` : Ajoute une transition de 300ms pour les changements de style.

## Conclusion

Le composant `BackButton` fournit un moyen élégant et fonctionnel pour permettre aux utilisateurs de revenir à la page précédente, en utilisant `react-router-dom` pour la navigation et Tailwind CSS pour le style. Ce composant peut être facilement intégré dans n'importe quelle partie de l'application nécessitant cette fonctionnalité.

---